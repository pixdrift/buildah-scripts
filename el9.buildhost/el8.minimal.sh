#!/bin/bash

#
# Generate minimal container image from EL8 repo on EL9 build host using buildah (https://github.com/projectatomic/buildah)
# Note: This is an updated script that uses dnf on El9 instead of yum
#

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount $newcontainer)
imagename=el8-minimal
repolabel=baseos-8

# install container packages using an EL8 repo
dnf --disablerepo="*" --enablerepo="${repolabel}" install --installroot $scratchmnt bash coreutils --releasever 8 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 --setopt=install_weak_deps=false -y

# clean up yum cache to save space
if [ -d "${scratchmnt}" ]; then
  rm -rvf "${scratchmnt}"/var/cache/dnf
fi

# configure container label and entrypoint
buildah config --label name=$imagename $newcontainer
buildah config --cmd /bin/bash $newcontainer

# commit the image
buildah unmount $newcontainer
buildah commit $newcontainer $imagename

# clean up the working container
buildah rm $newcontainer
