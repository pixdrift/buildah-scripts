#!/bin/bash

#
# Generate minimal container image with distribution provided Python and PIP (~110M) from CentOS8 repo using buildah (https://github.com/projectatomic/buildah)
#

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})
imagename=el8-minimal-python38

# install container packages using a CentOS 8 repo on CentOS 7 build server
yum --disablerepo="*" --enablerepo="8-stream*" --installroot ${scratchmnt} --releasever 8 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y install bash coreutils python38 python38-pip

# Clean up yum cache
[ -d "${scratchmnt}" ] && rm -r "${scratchmnt}"/var/cache/yum

# configure container label and default command
buildah config --label name=${imagename} ${newcontainer}
buildah config --cmd /bin/bash ${newcontainer}

# commit the image
buildah commit --rm ${newcontainer} ${imagename}