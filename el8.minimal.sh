#!/bin/bash

#
# Generate minimal container image (~57MB) from CentOS8 repo using buildah (https://github.com/projectatomic/buildah)
#

set -ex

# start new container from scratch
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# install container packages using a CentOS 8 repo on CentOS 7 build server
yum --disablerepo="*" --enablerepo="8-stream" install --installroot ${scratchmnt} bash coreutils --releasever 8 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# clean up yum cache to save space
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/yum
fi

# configure container label and entrypoint
buildah config --label name=el8-minimal ${newcontainer}
buildah config --cmd /bin/bash ${newcontainer}

# commit the image
buildah unmount ${newcontainer}
buildah commit ${newcontainer} el8-minimal

# clean up the working container
buildah rm ${newcontainer}