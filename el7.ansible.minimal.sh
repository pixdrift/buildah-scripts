#!/bin/bash
set -ex

if ! pip &>/dev/null; then
  echo "pip not found on host (yum install python2-pip?)"
  exit 1
fi

# Build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# Install base yum packages including python
yum install --installroot ${scratchmnt} bash coreutils python --releasever 7 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# Clean up yum cache
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/yum
fi

# Install pip packages using host pip installation (python2-pip)
pip install ansible==2.5.0 --prefix="${scratchmnt}" --ignore-installed --disable-pip-version-check --no-cache-dir

buildah config --cmd /bin/bash ${newcontainer}
buildah config --label name=ansible-minimal ${newcontainer}

buildah unmount ${newcontainer}
buildah commit ${newcontainer} ansible-minimal

# Clean up the working container
buildah rm ${newcontainer}